#!/bin/sh
#
# CA - wrapper around ca to make it easier to use ... basically ca requires
#      some setup stuff to be done before you can use it and this makes
#      things easier between now and when Eric is convinced to fix it :-)
#
# CA -newca ... will setup the right stuff
# CA -newreq ... will generate a certificate request
# CA -sign ... will sign the generated request and output
#
# At the end of that grab newreq.pem and newcert.pem (one has the key
# and the other the certificate) and cat them together and that is what
# you want/need ... I'll make even this a little cleaner later.
#
#
# 12-Jan-96 tjh    Added more things ... including CA -signcert which
#                  converts a certificate to a request and then signs it.
# 10-Jan-96 eay    Fixed a few more bugs and added the SSLEAY_CONFIG
#                  environment variable so this can be driven from
#                  a script.
# 25-Jul-96 eay    Cleaned up filenames some more.
# 11-Jun-96 eay    Fixed a few filename missmatches.
# 03-May-96 eay    Modified to use 'ssleay cmd' instead of 'cmd'.
# 18-Apr-96 tjh    Original hacking
#
# Tim Hudson
# tjh@cryptsoft.com
#

# default openssl.cnf file has setup as per the following
# demoCA ... where everything is stored
cp_pem() {
    infile=$1
    outfile=$2
    bound=$3
    flag=0
    exec <$infile;
    while read line; do
	if [ $flag -eq 1 ]; then
		echo $line|grep "^-----END.*$bound"  2>/dev/null 1>/dev/null
		if [ $? -eq 0 ] ; then
			echo $line >>$outfile
			break
		else
			echo $line >>$outfile
		fi
	fi

	echo $line|grep "^-----BEGIN.*$bound"  2>/dev/null 1>/dev/null
	if [ $? -eq 0 ]; then
		echo $line >$outfile
		flag=1
	fi
    done
}

usage() {
 echo "Usage: $0 action [arguments]\n" >&2
 echo "Actions:"
 echo "  -newca" >&2
 echo "  -newcert|-newreq|-newreq-nodes|-sign|-signcert|-signCA|"\
"-verify|-xsign [name]" >&2
 echo "  -pkcs12 [name] [title]" >&2
}

if ! $(command -v crudini > /dev/null 2>&1) ; then
    echo "crudini could not be found"
    exit 1
fi

if [ -z "$SSLEAY_CONFIG" ] ; then SSLEAY_CONFIG="openssl.cnf" ; fi
if [ ! -f "$SSLEAY_CONFIG" ] ; then
    echo "Configuration file $SSLEAY_CONFIG could not be found."
    exit 1
fi

if [ -z "$DAYS" ] ; then DAYS="-days 365" ; fi	# 1 year
CADAYS="-days 1095"	# 3 years
REQ="openssl req -config $SSLEAY_CONFIG"
CA="openssl ca -config $SSLEAY_CONFIG"
VERIFY="openssl verify"
X509="openssl x509"
PKCS12="openssl pkcs12"

CATOP=$(crudini --get $SSLEAY_CONFIG CA_default dir)
CAKEY=cakey.pem
CAREQ=careq.pem
CACERT=cacert.pem

RET=0

if [ -z "$1" ] ; then usage ; fi
while [ "$1" != "" ] ; do
case $1 in
-\?|-h|-help)
    usage
    exit 0
    ;;
-newcert)
    # create a certificate
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $REQ -new -x509 -keyout ${FNAME}key.pem -out ${FNAME}req.pem $DAYS
    RET=$?
    if [ $RET -eq 0 ] ; then
        echo "Request is in ${FNAME}req.pem, private key is in ${FNAME}key.pem"
    fi
    exit $RET
    ;;
-newreq)
    # create a certificate request
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $REQ -new -keyout ${FNAME}key.pem -out ${FNAME}req.pem $DAYS
    RET=$?
    if [ $RET -eq 0 ] ; then
        echo "Request is in ${FNAME}req.pem, private key is in ${FNAME}key.pem"
    fi
    exit $RET
    ;;
-newreq-nodes) 
    # create a certificate request
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $REQ -new -nodes -keyout ${FNAME}req.pem -out ${FNAME}req.pem $DAYS
    RET=$?
    if [ $RET -eq 0 ] ; then
        echo "Request (and private key) is in ${FNAME}req.pem"
    fi
    exit $RET
    ;;
-newca)
    # if explicitly asked for or it doesn't exist then setup the directory
    # structure that Eric likes to manage things
    if [ ! -f ${CATOP}/serial ]; then
	# create the directory hierarchy
        mkdir -p ${CATOP} ${CATOP}/certs ${CATOP}/crl ${CATOP}/newcerts \
                 ${CATOP}/private
	touch ${CATOP}/index.txt
    fi
    if [ ! -f ${CATOP}/private/$CAKEY ]; then
	echo "CA certificate filename (or enter to create)"
	read FILE

	# ask user for existing CA certificate
	if [ "$FILE" ]; then
	    cp_pem $FILE ${CATOP}/private/$CAKEY PRIVATE
	    cp_pem $FILE ${CATOP}/$CACERT CERTIFICATE
	    RET=$?
	    if [ ! -f "${CATOP}/serial" ]; then
		$X509 -in ${CATOP}/$CACERT -noout -next_serial \
		      -out ${CATOP}/serial
	    fi
	else
	    echo "Making CA certificate ..."
	    $REQ -new -keyout ${CATOP}/private/$CAKEY \
			   -out ${CATOP}/$CAREQ
	    $CA -create_serial -out ${CATOP}/$CACERT $CADAYS -batch \
			   -keyfile ${CATOP}/private/$CAKEY -selfsign \
			   -extensions v3_ca \
			   -infiles ${CATOP}/$CAREQ
	    RET=$?
	fi
    fi
    ;;
-xsign)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $CA -policy policy_anything -infiles ${FNAME}req.pem
    RET=$?
    ;;
-pkcs12)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    if [ -z "$3" ] ; then
	CNAME="My Certificate"
    else
	CNAME="$3"
    fi
    $PKCS12 -in ${FNAME}cert.pem -inkey ${FNAME}key.pem -certfile ${CATOP}/$CACERT \
	    -out ${FNAME}cert.p12 -export -name "$CNAME"
    RET=$?
    exit $RET
    ;;
-sign|-signreq)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $CA -policy policy_anything -out ${FNAME}cert.pem -infiles ${FNAME}req.pem
    RET=$?
    if [ $RET -eq 0 ] ; then
        cat ${FNAME}cert.pem
        echo "Signed certificate is in ${FNAME}cert.pem"
    fi
    exit $RET
    ;;
-signCA)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $CA -policy policy_anything -out ${FNAME}cert.pem -extensions v3_ca -infiles ${FNAME}req.pem
    RET=$?
    if [ $RET -eq 0 ] ; then
        echo "Signed CA certificate is in ${FNAME}cert.pem"
    fi
    exit $RET
    ;;
-signcert)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    echo "Cert passphrase will be requested twice - bug?"
    $X509 -x509toreq -in ${FNAME}req.pem -signkey ${FNAME}key.pem -out tmp.pem
    $CA -policy policy_anything -out ${FNAME}cert.pem -infiles tmp.pem
    RET=$?
    if [ $RET -eq 0 ] ; then
        cat ${FNAME}cert.pem
        echo "Signed certificate is in ${FNAME}cert.pem"
    fi
    exit $RET
    ;;
-verify)
    if [ -z "$2" ] ; then
        FNAME="new"
    else
        FNAME="$2"
    fi
    $VERIFY -CAfile $CATOP/$CACERT ${FNAME}cert.pem
    RET=$?
    exit $RET
    ;;
*)
    usage
    exit 1
    ;;
esac
shift
done
exit $RET
